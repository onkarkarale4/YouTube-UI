/* eslint-disable no-unused-vars */
import React from 'react'
import ButtonList from '../MainContainer/ButtonList'
import VideoContainer from '../MainContainer/VideoContainer'

const MainContainer = () => {
  return (
    <div>
      <ButtonList />
      <VideoContainer />
    </div>
  )
}

export default MainContainer
